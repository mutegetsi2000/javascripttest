const router = require('express').Router()
const {provModel,provschema} = require('../models/prov.model')

router.get('/',async (req,res)=>{
    const prov =await provModel.find()
    res.status(200).send(prov)
})

router.post('/add',async (req,res)=>{
   const {error,value} = provschema.validate(req.body)
   if(error){
     res.status(400).send(error.details[0].message)
     return 
   }
   try {
    const result = await new provModel(value).save()
    res.status(201).send(result)
   } catch (error) {
       const err = new Error("database is failing")
       err.db = "this is db error"
   }
    
   
})


router.put('/update/:name',async (req,res)=>{
   const name = req.params.name;
   const {error,value} = provschema.validate(req.body)
   if(error){
       return res.status(400).send(error.details[0].message)
   }
   try {
    const result = await provModel.findOneAndUpdate({name:name},value)
    res.status(201).send(result._update)
   } catch (error) {
    const err = new Error("database is failing")
    err.db = "this is db error"
   }
   
})


router.delete('/remove/:name',(req,res)=>{
    const name = req.params.name;
    const result = provModel.findOneAndDelete({name:name})
    try {
        
        res.status(201).send(`${result._conditions.name} province is deleted`)
    } catch (error) {
        const err = new Error("database is failing")
       err.db = "this is db error"
    }
})

module.exports = router