const mongoose = require('mongoose')
const config = require('config')

const conn = mongoose.connect(`mongodb://${config.get('db.host')}/${config.get('db.name')}`,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useFindAndModify:false
},(err,res)=>{
    if(err){
        err.db="this is the db error"
        return;
    }
    console.log(`db connected succesfully`)
})
module.exports = conn;

