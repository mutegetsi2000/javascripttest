const {Schema,model} = require('mongoose')
const Joi = require('joi')

const provSchema = new Schema({
    name:{type:String,required:true},
    governor:{type:String,required:true},
    hQ:{type:String},
    numDistrict:{type:String}
})

module.exports.provschema = Joi.object().keys({
    name: Joi.string().min(5).required(),
    governor:Joi.string().required(),
    hQ:Joi.string(),
    numDistrict:Joi.string().required()
})
module.exports.provModel = model('province',provSchema)


const distrSchema = new Schema({
    name:{type:String,required:true},
    mayor:{type:String,required:true},
    hQ:{type:String},
    province:{type:String}
})
 module.exports.distrschema = Joi.object().keys({
    name: Joi.string().min(5).required(),
    mayor:Joi.string().required(),
    hQ:Joi.string(),
    province:Joi.string().required()
})
module.exports.distrModel = model('district',distrSchema)