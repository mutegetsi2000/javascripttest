const express = require('express')

const app = express()
const config = require('config')
const debug = require('debug') 
const appApis = debug('app:debug')
const districtController = require('./controllers/districtController')
const provinceController = require('./controllers/provinceController')
require('./models/conn')
const dbErrors = require('debug')('db:error')
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use((err,req,res,next)=>{
    if(err.db){
      dbErrors('This problem is due to database')
    }else{
        next()
    }
})
app.get('/',(req,res)=>{
    res.send("Welcome to the Api")
})

app.use((req,res,next)=>{
    appApis(req.method)
    next()
})
app.use('/district',districtController)
app.use('/province',provinceController)

const port = config.get('app.port')
app.listen(port,_=>appApis(`connected succesfully on port ${port}`))

